package libro.springboot.piola.biblioteca;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LibroServiceImpl implements ILibroService {

  @Autowired
  private LibroRepository libroRepository;

  @Override
  public List<Libro> findAll() {
    return libroRepository.findAll();
  }

  @Override
  public Libro findById(int id) {
    if (id == 9999)
      return new Libro(9999, "", "Especial", "");
    else
      return libroRepository.findById(id);
  }

  @Override
  public void addLibro(LibroDTO libroDTO) {    
    Libro libro = new Libro(libroDTO.getId(), libroDTO.getISBN(), libroDTO.getNombreObra(), libroDTO.getAutor());
    libroRepository.addLibro(libro);    
  }
}
