package libro.springboot.piola.biblioteca;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Component;

@Component
public class LibroRepository {

  private List<Libro> libros;

  public List<Libro> CargaBDLibros() {
    libros = new ArrayList<Libro>();
    libros.add(new Libro(1, "123C", "Java para novatines", "HGC"));
    libros.add(new Libro(2, "444A", "Net Z", "Juan K."));
    libros.add(new Libro(3, "54FN", "Don Quijoteitor 2000", "Pedro P."));
    libros.add(new Libro(4, "TTY7", "Programación a todo gas", "JP"));
    return libros;
  }

  public List<Libro> findAll() {
    return CargaBDLibros();
  }

  public Libro findById(int id) {
    CargaBDLibros();
    for (Libro libro : libros) {
      if (libro.getId() == id) {
        return libro;
      }
    }
    return null;
  }

  public void addLibro(Libro libro) {
    CargaBDLibros();
    libros.add(libro);
  }


}
