package libro.springboot.piola.biblioteca;
import java.util.List;

public interface ILibroService {    
    
    Libro findById(int id);
    List<Libro> findAll();
    void addLibro(LibroDTO libroDTO);
}
