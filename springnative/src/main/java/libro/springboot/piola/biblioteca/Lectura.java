package libro.springboot.piola.biblioteca;

import lombok.*;

/**
 * Identificamos una lectura
 */

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Lectura {
  private int id;
  private int idUsuario;
  private String idLibro;
  private int estado; // 0 sin leer. 1 leyendo, 2 terminado
}