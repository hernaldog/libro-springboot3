package libro.springboot.piola.biblioteca;
import jakarta.validation.constraints.*;
import lombok.*;

/* DTO de un libro */

@Value
@AllArgsConstructor
public class LibroDTO {
  @Positive(message = "Ingrese un ID válido por favor")
  private int id;
  @NotBlank
  private String ISBN;
  @NotBlank
  private String nombreObra;
  @NotEmpty
  private String autor;
  @Min(1) @Max(2030)
  private Integer agnoPublicacion;
  @Positive
  private Integer estado;
}
