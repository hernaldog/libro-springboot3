package libro.springboot.piola.biblioteca;
import java.util.List;
import lombok.Value;

/* DTO con listado de libros */

@Value
public class BibliotecaDTO {
  List<LibroDTO> libros;
}
