package libro.springboot.piola.biblioteca;
import lombok.*;

/**
 * Identificamos un libro
 */

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Libro {
    private int id;
    private String ISBN;
    private String nombreObra;
    private String autor;
}
