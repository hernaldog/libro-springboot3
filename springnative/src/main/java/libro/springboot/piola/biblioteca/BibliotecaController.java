package libro.springboot.piola.biblioteca;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import jakarta.validation.Valid;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/biblioteca")
public class BibliotecaController {
  private final ILibroService ilibroService;
  
  @GetMapping("/libro/{id}")
  ResponseEntity<Libro> findLibroById(@PathVariable int id) {
    Libro libro = ilibroService.findById(id);
    return ResponseEntity.ok(libro);
  }

  @PostMapping("/add-libro")
  void addLibro(@RequestBody @Valid LibroDTO libroDTO) {
    ilibroService.addLibro(libroDTO);
  }  

  @GetMapping("/libros") // todos
  List<Libro> findAllLibros() {
    List<Libro> libros = ilibroService.findAll();
    log.info("Buscando todos los libros: {}", libros);
    return libros;
  }
}