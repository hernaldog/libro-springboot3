package libro.springboot.piola;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PiolaApplication {

  public static void main(String[] args) {
    SpringApplication.run(PiolaApplication.class, args);
  }

  /* No usao, solo para ejemplo del capítulo 3 */
  /*
  @Bean
  public ObjectMapper objectMapper() {
    var om = new ObjectMapper();    
    om.setPropertyNamingStrategy(PropertyNamingStrategies.SNAKE_CASE);
    return om;
  }
  */
}
