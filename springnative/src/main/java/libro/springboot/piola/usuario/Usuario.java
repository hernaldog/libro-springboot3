package libro.springboot.piola.usuario;

import lombok.*;

/**
 * Identificamos un usuario
 */

@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
public class Usuario {
  private Long id;
  private String correo;
  private String alias;
}
