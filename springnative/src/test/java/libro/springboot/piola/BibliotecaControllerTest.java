package libro.springboot.piola;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJsonTesters;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import libro.springboot.piola.biblioteca.BibliotecaController;
import libro.springboot.piola.biblioteca.ILibroService;
import libro.springboot.piola.biblioteca.Libro;
import libro.springboot.piola.biblioteca.LibroDTO;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@WebMvcTest(BibliotecaController.class)
@AutoConfigureJsonTesters
public class BibliotecaControllerTest {

  @Autowired
  private MockMvc mvc;

  @Autowired  
  private JacksonTester<Libro> jsonResultadoLibro;
  
  @Autowired
  private JacksonTester<LibroDTO> jsonRequestAttempt;

  @MockBean // interfaz del servicio ya que el Controller lo usa
  private ILibroService libroService;

 @Test
  void getFindByIdOk() throws Exception {

    // given    
    Libro libroEsperado = new Libro(10, "55ABC", "Super javaman", "hp");

    given(libroService.findById(10)).willReturn(libroEsperado);

    // when
    MockHttpServletResponse response = mvc.perform(get("/biblioteca/libro/{id}", 10)
        .contentType(MediaType.APPLICATION_JSON))
        .andReturn().getResponse();

    // then
    then(response.getStatus()).isEqualTo(HttpStatus.OK.value());
    then(response.getContentAsString()).isEqualTo(jsonResultadoLibro.write(libroEsperado).getJson());
  }

  @Test
  void postAddLibroNoOk() throws Exception {

    // given  libro con año inválido -1 y nombre obra vacío ("")
    LibroDTO libroEntradaDTO = new LibroDTO(888,"9999ABC", "", "naldo", -1, 0);

    // when
    MockHttpServletResponse response = mvc.perform(post("/biblioteca/add-libro")
        .contentType(MediaType.APPLICATION_JSON)
        .content(jsonRequestAttempt.write(libroEntradaDTO).getJson()))
        .andReturn().getResponse();

    // then Se espera código 400 si se envía un valor inválido de entrada en el DTO
    then(response.getStatus()).isEqualTo(HttpStatus.BAD_REQUEST.value());
  }
}