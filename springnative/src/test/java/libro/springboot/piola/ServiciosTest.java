package libro.springboot.piola;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import libro.springboot.piola.biblioteca.LibroRepository;
import libro.springboot.piola.biblioteca.LibroServiceImpl;
import libro.springboot.piola.biblioteca.Libro;

import java.util.ArrayList;
import java.util.List;
import static org.mockito.BDDMockito.given;
import static org.assertj.core.api.BDDAssertions.then;

@ExtendWith(MockitoExtension.class)
public class ServiciosTest {

  @Mock  //clases a mockear
  private LibroRepository libroRepository;

  @InjectMocks  //servicio que se quieren validar
  private LibroServiceImpl libroService;

  private List<Libro> librosBase;

  @BeforeEach  //configuración que se ejecuta antes de cada test
  public void setUp() {
    librosBase = new ArrayList<Libro>();
    librosBase.add(new Libro(1, "test1", "test1", "autor1"));
    librosBase.add(new Libro(2, "test2", "test2", "autor2"));
    librosBase.add(new Libro(99, "test3", "test3", "autor3"));
  }

  @Test
  public void findall() {

    // Given (dado) para pre-condiciones    
    given(libroRepository.findAll()).willReturn(librosBase);

    // When (cuando) para cuando ocurra una acción -> no usado

    // Then (entonces) para verificar
    var librosTest = libroService.findAll();
    then(librosTest).isNotNull();
  }

  
  @Test
  public void findById() {
    // Given (dado)
    given(libroRepository.findById(100)).willReturn(new Libro(100, "123", "test", "test"));

    // Then (entonces)
    Libro libroTest = libroService.findById(100);
    then(libroTest).isNotNull(); // la salida no debe ser cero
  }

  @Test
  public void findByIdNoEncontrado() {
    // Then (entonces)
    Libro libroTest = libroService.findById(10);
    then(libroTest).isNull();  // salida null
  }

  @Test
  public void findByIdEspecial() {    
    // Then (entonces)
    Libro libroTest = libroService.findById(9999);
    then(libroTest).isEqualTo(new Libro(9999, "", "Especial", ""));
  }


}